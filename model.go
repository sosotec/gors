package gors

import (
	"github.com/go-redis/redis"
	"github.com/yanyiwu/gojieba"
	"regexp"
)

type hit struct {
	doc     *Document
	matches []*match
	factor  *factor
}

type factor struct {
	Rank  int          `json:"rank"`
	Rate  int          `json:"rate"`
	Mask  int          `json:"mask"`
	Order *orderFactor `json:"order"`
}

type match struct {
	Word string `json:"word"`
	Rate int    `json:"rate"`
	Mask int    `json:"mask"`
}

type result struct {
	Doc    *Document `json:"doc"`
	Words  []string  `json:"words"`
	Factor *factor   `json:"factor"`
}

type replaceRule struct {
	rule *regexp.Regexp
	text string
}

type orderRule struct {
	rule  *regexp.Regexp
	value string
}

type orderFactor struct {
	group int
	sort  float64
}

type Document struct {
	Id   interface{}            `json:"id"`
	Text string                 `json:"text"`
	Data map[string]interface{} `json:"data"`
}

type Option struct {
	Filter       map[string]interface{}
	Limit        int
	OutputFactor bool
}

type Client struct {
	db  *redis.Client
	jba *gojieba.Jieba
	rps []replaceRule
	ods []orderRule
}
