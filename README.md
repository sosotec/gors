# GoRS

#### 介绍
Go +Redis，使用中文分词， 实现的小型的搜索引擎

#### 配置文件
在应用根目录下面，放置一个 gors.yaml 文件

``` yaml
redis:
  db: 0  //储存的RedisDB位置，默认是1
  host: "127.0.0.1:6379"
  passwd: ""

replace: //搜索输入进行前置替换，使用正则表达式
  "(\d+)p": "$1 plus" //如输入 7p，则替换成 7 plus 进行搜索
  "([a-z]{2,})(\d+)": "$0 $1 $2" //如输入 nova5，则替换成nova5 nova 5 （三个复合）进行搜索
  "苹果\s*([a-z]+)": "iPhone $1" //输入 苹果7，则替换成iPhone 7 进行搜索
  "苹果": "iPhone"
  "x": "iPhone X"

order: //二次排序因数，由两个数组成，分组+排序（由大至小），分组可把相同品牌归纳在一起
  "iPhone SE": "100,10"
  "iPhone X": "100,10"
  "iPhone (\d+)": "100,$1"
  "OPPO[^\d]+(\d+)": "80,$1"
  "VIVO[^\d]+(\d+)": "70,$1"

```

#### 填充示例


```golang
package main

import (
	"fmt"
	"strings"
	"gitee.com/sosotec/gors"
)

func main() {
	
	//初始化连接
	conn := gors.Connect()

	//执行搜索
	var err = conn.Fill(gors.Document{
		Id:   "1331",                  //数据主键，可字符、可数字
		Text: "中华人民共和国解放军",    //需要被搜索到的文本内容，默认不含HTML符号
		Data: map[string]interface{}{  //自定义的数据，使用MAP类似输入
			"type":  2,
			"value": "testing value 1",
		},
	})
	if err != nil {
		fmt.Printf(err.Error())
    }
	
}
```

#### 删除示例

```golang
package main

import (
	"fmt"
	"strings"
	"gitee.com/sosotec/gors"
)

func main() {

	//初始化连接
	conn := gors.Connect()

	//执行搜索
	var err = conn.Delete("1331")  //传入数据主键ID
	if err != nil {
		fmt.Printf(err.Error())
	}

}
```

#### 搜索示例

```golang
package main

import (
	"fmt"
	"strings"
	"gitee.com/sosotec/gors"
)

func main() {
	
	//初始化连接
	conn := gors.Connect()

	//搜索条件
	var opt = gors.Option{
		Limit:        20,
		OutputFactor: true,
	}

	//搜索词
	var text = "苹果"

	//执行搜索
	var res, err = conn.Search(text, opt)
	
}
```