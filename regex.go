package gors

import "regexp"

var sectionRegex, _ = regexp.Compile(`^[a-zA-Z]+:$`)
var partRegex, _ = regexp.Compile(`^([^:]+):(.+)$`)
var numberRegex, _ = regexp.Compile(`^\d+$`)
