package gors

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/yanyiwu/gojieba"
	"io"
	"os"
	"regexp"
	"sort"
	"strings"
)

func connectRedis(conf map[string]string) *redis.Client {

	//实例化
	var db = redis.NewClient(&redis.Options{
		Addr:     conf["host"],            //server
		Password: conf["passwd"],          // password set
		DB:       stringToInt(conf["db"]), // use default DB
	})

	//检查是否已经连通
	pong, err := db.Ping().Result()
	if err != nil {
		panic(err)
	} else {
		fmt.Printf("Redis connect ping success: %s \n", pong)
	}

	//返回
	return db
}

func loadConfig() map[string]map[string]string {

	//加载配置文件
	file, ex := os.Open("./gors.yaml")
	if ex != nil {
		panic(ex)
	}

	// 创建 Reader
	var reader = bufio.NewReader(file)
	var line string
	var err error

	//初始化配置容器
	var conf = map[string]map[string]string{}
	var section string
	var parts [][]string
	var key string
	var val string

	//循环处理
	for {

		//读取行
		line, err = reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}

		//检查是否为部分节点
		if sectionRegex.MatchString(line) {
			section = strings.TrimRight(line, ":")
			if _, ok := conf[section]; !ok {
				conf[section] = map[string]string{}
			}
			continue
		}

		//压入配置节点
		if section == "" {
			continue
		}
		parts = partRegex.FindAllStringSubmatch(line, -1)
		if parts == nil {
			continue
		}

		key = strings.TrimSpace(parts[0][1])
		key = strings.ReplaceAll(key, "\"", "")
		val = strings.TrimSpace(parts[0][2])
		val = strings.ReplaceAll(val, "\"", "")

		conf[section][key] = val
	}

	//返回
	return conf
}

func convertReplace(parts map[string]string) []replaceRule {
	var list []replaceRule
	for rule, text := range parts {
		var regex, _ = regexp.Compile(fmt.Sprintf("^(?i)%s$", rule))
		list = append(list, replaceRule{
			rule: regex,
			text: text,
		})
	}
	return list
}

func convertOrder(parts map[string]string) []orderRule {
	var list []orderRule
	for rule, value := range parts {
		var regex, _ = regexp.Compile("(?i)" + rule)
		list = append(list, orderRule{
			rule:  regex,
			value: strings.ReplaceAll(value, " ", ""),
		})
	}
	return list
}

func appendResult(res []result, list hits, limit int, outputFactor bool) ([]result, bool) {
	var stop = false
	var left = limit - len(res)
	if left == 0 {
		return res, true
	}
	for idx, item := range list {
		var words []string
		for _, mt := range item.matches {
			words = append(words, mt.Word)
		}
		var rel = result{
			Doc:   item.doc,
			Words: words,
		}
		if outputFactor {
			rel.Factor = item.factor
		}
		res = append(res, rel)
		if idx >= (left - 1) {
			stop = true
			break
		}
	}
	return res, stop
}

func Connect(dbIndex ...int) *Client {

	//加载配置
	var conf = loadConfig()
	if _, ok := conf["redis"]; ok && len(dbIndex) > 0 {
		conf["redis"]["db"] = fmt.Sprintf("%v", dbIndex[0])
	}

	//实例化
	var db = connectRedis(conf["redis"])

	//初始化分词
	var jba = gojieba.NewJieba()

	//搜索词替换引擎
	var crp = convertReplace(conf["replace"])

	//搜索结果排序因数
	var cod = convertOrder(conf["order"])

	//创建客户端
	clt := &Client{
		db:  db,
		jba: jba,
		rps: crp,
		ods: cod,
	}

	//返回
	return clt
}

// Fill 填充内容至搜索引擎内
func (c *Client) Fill(doc Document) (err error) {

	//检查是否有值
	if doc.Text == "" {
		err = errors.New("document's test is empty")
		return
	}

	//原始值
	id := fmt.Sprintf("%v", doc.Id)
	now := currentTime()

	//检查是否有二次排序因数
	var order = ""
	if len(c.ods) > 0 {
		var mts [][]string
		var count int
		for _, item := range c.ods {
			mts = item.rule.FindAllStringSubmatch(doc.Text, -1)
			if mts == nil {
				continue
			}
			count = len(mts[0])
			if count == 1 {
				order = item.value
			} else if count == 2 {
				order = strings.Replace(item.value, "$1", mts[0][1], -1)
			}
			break
		}
	}

	//Redis储存原始内容
	orgVal := md5Encode(id)
	orgKey := fmt.Sprintf("g-o-%v", orgVal)
	relKey := fmt.Sprintf("g-r-%v", orgVal)
	datKey := fmt.Sprintf("g-d-%v", orgVal)
	c.db.HSet(orgKey, "id", id)
	c.db.HSet(orgKey, "text", doc.Text)
	c.db.HSet(orgKey, "time", now)

	//如果有自定义data
	if doc.Data != nil {
		dat := jsonToString(doc.Data)
		if dat != "" && dat != "{}" && dat != "[]" {
			c.db.HSet(orgKey, "data", dat)
		}
	}

	//如果有自定义order
	if order != "" {
		var ods = strings.Split(order, ",")
		if len(ods) == 2 {
			c.db.HSet(orgKey, "group", ods[0])
			c.db.HSet(orgKey, "sort", ods[1])
		}
	}

	//声明字段
	var wordVal string
	var wordKey string
	var newText string
	var datVal string
	var len1 = float64(len(doc.Text)) //原文长度
	var len2 float64 = 0              //支掉分词后的字词长度
	var rate = 0                      //分词覆盖率

	//精确模式
	cuts := c.jba.CutForSearch(doc.Text, true)
	for _, word := range cuts {

		//如果是空格
		word = strings.TrimSpace(word)
		if word == "" || word == "　" {
			continue
		}

		//Redis保存KEY
		wordVal = md5Encode(word)
		wordKey = fmt.Sprintf("g-w-%v", wordVal)

		//计算本词在原文的覆盖比
		newText = strings.ReplaceAll(doc.Text, word, "")
		len2 = float64(len(newText))
		rate = 100 - int(len2*100/len1)

		//保存至Redis
		c.db.HSet(wordKey, orgVal, rate) //保存分词与原文的关系（含匹配覆盖率），用于通过分词搜索出关联的原文
		c.db.HSet(relKey, wordVal, 0)    //保存原文与分词的关系，用于通过原文删除所属的分词储存
	}

	//保存原与与data之间的kv关键，用于搜索区分使用
	for key, val := range doc.Data {
		datVal = md5Encode(fmt.Sprintf("%s:%v", key, val))
		c.db.HSet(datKey, datVal, 0)
	}

	//返回
	return
}

// Delete 通过指定的数据ID删除搜索内存
func (c *Client) Delete(id interface{}) (err error) {

	//原始值
	idTxt := fmt.Sprintf("%v", id)
	if idTxt == "" {
		err = errors.New("document's id is empty")
		return
	}

	//Redis KEY
	var orgVal = md5Encode(idTxt)
	var orgKey = fmt.Sprintf("g-o-%v", orgVal) //原文KEY
	var relKey = fmt.Sprintf("g-r-%v", orgVal) //原文下储存分词的KEY
	var datKey = fmt.Sprintf("g-d-%v", orgVal) //原文下储存DATA节内的KEY
	var wordKey string                         //分词储存的KEY

	//删除分词
	fields, _ := c.db.HGetAll(relKey).Result()
	for val, _ := range fields {
		wordKey = fmt.Sprintf("g-w-%v", val)
		c.db.HDel(wordKey, orgVal)
	}

	//删除原文
	c.db.Del(orgKey)
	c.db.Del(relKey)
	c.db.Del(datKey)

	//返回
	return
}

// Search 通过文本内容搜索，opt可指定输出数量和过滤的数据
func (c *Client) Search(text string, opt ...*Option) (res []result, err error) {

	//检查是否有值
	if text == "" {
		err = errors.New("document's test is empty")
		return
	}

	//对搜索替换进行处理
	for _, cvt := range c.rps {
		mts := cvt.rule.FindAllStringSubmatch(text, -1)
		if mts == nil {
			continue
		}
		ntx := cvt.text
		for i := 0; i < len(mts[0]); i++ {
			ntx = strings.ReplaceAll(ntx, fmt.Sprintf("$%v", i), mts[0][i])
		}
		text = ntx
		break
	}

	//声明字段
	var wordKey string
	var newText string
	var datVal string
	var datKey string
	var filter bool
	var len1 = float64(len(text))
	var len2 float64
	var mask int

	//是否有外部参数
	var outputFactor = false
	var filters []string
	var limit = 50
	if len(opt) > 0 {
		if len(opt[0].Filter) > 0 {
			for key, val := range opt[0].Filter {
				datVal = md5Encode(fmt.Sprintf("%s:%v", key, val))
				filters = append(filters, datVal)
			}
		}
		if opt[0].Limit > 0 {
			limit = opt[0].Limit
		}
		outputFactor = opt[0].OutputFactor
	}

	//搜索到的 map[原文KEY]命中次数
	orgs := map[string][]*match{}

	//精确模式
	cuts := c.jba.CutForSearch(text, true)
	for _, word := range cuts {

		//如果是空格
		word = strings.TrimSpace(word)
		if word == "" || word == "　" {
			continue
		}

		//计算本词在搜索的覆盖比
		newText = strings.ReplaceAll(text, word, "")
		len2 = float64(len(newText))
		mask = 100 - int(len2*10/len1)

		//Redis保存KEY
		wordKey = fmt.Sprintf("g-w-%v", md5Encode(word))

		//从缓存中提取
		fields, _ := c.db.HGetAll(wordKey).Result()

		//处理每个搜索出来的结果
		for org, rate := range fields {

			//如果有过滤条件
			if len(filters) > 0 {
				filter = true
				datKey = fmt.Sprintf("g-d-%v", org)
				for _, dat := range filters {
					if r, _ := c.db.HGet(datKey, dat).Result(); r != "0" {
						filter = false
						break
					}
				}
				if filter == false {
					continue
				}
			}

			//匹配对象
			mt := &match{
				Word: word,
				Rate: stringToInt(rate),
				Mask: mask,
			}

			//记录匹配内容
			if vl, ok := orgs[org]; !ok {
				orgs[org] = []*match{mt}
			} else {
				orgs[org] = append(vl, mt)
			}
		}
	}

	//二次排序需要的分组容器
	var groupA = hits{} //标识mask为20000的
	var groupB = hits{} //标识mask为10000的
	var groupC = hits{} //符合含有二次排序的
	var groupD = hits{} //不符合二次排序的

	//补充数据输出
	var remove bool
	var orgVal string
	var txt1 string
	var txt2 string
	var rate int
	var rank int
	for org, mts := range orgs {

		//本原文关键词覆盖率
		rate = 0
		mask = 0

		//从中去掉关键词重复包含的情况，如 长江、大桥、长江大桥，需要把长江、大桥两个分词去掉
		var matches []*match
		for _, mv1 := range mts {
			remove = false
			for _, mv2 := range mts {
				if mv1.Word == mv2.Word {
					continue
				}
				if strings.Index(mv2.Word, mv1.Word) > -1 {
					remove = true
					break
				}
			}
			if remove == false {
				rate += mv1.Rate
				mask += mv1.Mask
				matches = append(matches, mv1)
			}
		}

		//原文KEY
		orgVal = fmt.Sprintf("g-o-%v", org)

		//从缓存中提取
		fields, _ := c.db.HGetAll(orgVal).Result()
		if len(fields) == 0 {
			continue
		}

		//组装数据
		var data map[string]interface{}
		stringToJson(fields["data"], &data)
		doc := &Document{
			Id:   fields["id"],
			Text: fields["text"],
			Data: data,
		}

		//判断搜索词在原文中的是否全匹配
		txt1 = strings.ToLower(doc.Text)
		txt2 = strings.ToLower(text)
		if txt1 == txt2 {
			mask = 20000
		} else if strings.Index(txt1, txt2) > -1 {
			mask = 10000
		}

		//默认排序
		rank = rate * mask

		//二次排序因子
		var order *orderFactor
		if od, ok := fields["group"]; ok {
			order = &orderFactor{
				group: stringToInt(od),
				sort:  stringToFloat64(fmt.Sprintf("%s.%v", fields["sort"], rank)),
			}
		}

		//组装数据
		dt := hit{
			doc:     doc,
			matches: matches,
			factor: &factor{
				Rate:  rate,
				Mask:  mask,
				Order: order,
				Rank:  rank,
			},
		}

		//分类归组
		if mask == 20000 {
			groupA = append(groupA, dt)
		} else if mask == 10000 {
			groupB = append(groupB, dt)
		} else if order != nil {
			groupC = append(groupC, dt)
		} else {
			groupD = append(groupD, dt)
		}
	}

	//排序，根据sort的值由高至低排
	sort.Sort(groupA)
	sort.Sort(groupB)
	sort.Sort(groupC)
	sort.Sort(groupD)

	//重组输出
	var stop bool
	res = []result{}
	if stop == false {
		res, stop = appendResult(res, groupA, limit, outputFactor)
	}
	if stop == false {
		res, stop = appendResult(res, groupB, limit, outputFactor)
	}
	if stop == false {
		res, stop = appendResult(res, groupC, limit, outputFactor)
	}
	if stop == false {
		res, stop = appendResult(res, groupD, limit, outputFactor)
	}

	//返回
	return
}
