package gors

type hits []hit

func (t hits) Len() int {
	return len(t)
}
func (t hits) Less(i, j int) bool {
	if t[i].factor.Order != nil && t[j].factor.Order != nil {
		var g1 = t[i].factor.Order.group
		var g2 = t[j].factor.Order.group
		if g1 > g2 {
			return true
		} else if g1 == g2 {
			return t[i].factor.Order.sort > t[j].factor.Order.sort
		}
	}
	return t[i].factor.Rank > t[j].factor.Rank
}
func (t hits) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}
