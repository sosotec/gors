package gors

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"
)

func md5Encode(str string) string {
	h := md5.New()
	h.Write([]byte(strings.ToLower(str)))
	return hex.EncodeToString(h.Sum(nil))
}

func jsonToString(val interface{}) string {
	data, err := json.Marshal(val)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	return string(data)
}

func stringToJson(val string, target interface{}) {
	if val == "" || val == "[]" || val == "{}" {
		return
	}
	data := []byte(val)
	err := json.Unmarshal(data, target)
	if err != nil {
		fmt.Println(err)
	}
}

func stringToInt(val string) int {
	id, err := strconv.Atoi(val)
	if err != nil {
		return 0
	}
	return id
}

func stringToFloat64(val string) float64 {
	id, err := strconv.ParseFloat(val, 64)
	if err != nil {
		return 0.0
	}
	return id
}

func currentTime() int64 {
	return time.Now().UnixNano() / 1e6
}
