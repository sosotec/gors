module gors

go 1.20

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/yanyiwu/gojieba v1.3.0
)

require (
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.27.5 // indirect
)
